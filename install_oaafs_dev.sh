echo -e "\e[1;33m - Create Dev repository \e[0m"
mkdir ~/oaafs_dev

echo -e "\e[1;33m - Get CoopItEasy migration folder \e[0m"
cd ~/oaafs_dev && git clone https://gitlab.com/coopiteasy/migration
mv migration coopiteasy-migration

echo -e "\e[1;33m - Install Commown framework \e[0m"
cd ~/oaafs_dev/ && git clone https://gitlab.com/oaafs/odoo-migrate-commown
cd ~/oaafs_dev/odoo-migrate-commown/migrate_tools
virtualenv env --python=python3
./env/bin/python -m pip install -r ../requirements.txt 
