echo -e "\e[1;33m - Full update of the system \e[0m"
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get full-upgrade -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y

echo -e "\e[1;33m - Installation of Sublim Text \e[0m"
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo add-apt-repository "deb https://download.sublimetext.com/ apt/stable/"
sudo apt-get update
sudo apt-get install sublime-text -y

echo -e "\e[1;33m - Installation of NextCloud \e[0m"
sudo add-apt-repository ppa:nextcloud-devs/client -y
sudo apt-get update
sudo apt-get install nextcloud-client -y

echo -e "\e[1;33m - Installation of Thunderbird \e[0m"
sudo snap install thunderbird -y


echo -e "\e[1;33m - Installation of Peek (Gif Capture) \e[0m"
sudo add-apt-repository ppa:peek-developers/stable -y
sudo apt-get update
sudo apt-get install peek -y

echo -e "\e[1;33m - Installation of Supervision Tools \e[0m"
sudo apt-get install htop -y

echo -e "\e[1;33m - Installation of Developper Tools \e[0m"
sudo apt-get install vim -y
sudo apt-get install git -y
sudo apt-get install python3-pip -y
sudo apt-get install terminator -y
sudo apt-get install ansible -y
sudo apt-get install postgresql-server-dev-12 -y
sudo apt-get install network-manager-openvpn-gnome -y
sudo apt-get install filezilla -y
sudo apt-get install keepassxc -y
sudo apt-get install kolourpaint -y
sudo apt-get install chromium-browser -y
sudo apt-get install meld -y

echo -e "\e[1;33m - Installation of LibreOffice \e[0m"
sudo add-apt-repository ppa:libreoffice/ppa -y
sudo apt-get update -y
sudo apt-get install libreoffice -y

echo -e "\e[1;33m - Installation of Ubuntu Tools \e[0m"
sudo apt-get install gnome-tweaks -y

echo -e "\e[1;33m - Installation of VPN Tools \e[0m"
sudo apt-get install openvpn resolvconf network-manager-openvpn network-manager-openvpn-gnome -y

echo -e "\e[1;33m - Unistallation of some undesired librairies \e[0m"
# https://forum.ubuntu-fr.org/viewtopic.php?id=2027835
sudo apt-get remove speech-dispatcher -y

echo -e "\e[1;33m - Git Configuration \e[0m"
git config --global user.email "sylvain.legal@grap.coop"
git config --global user.name "Sylvain LE GAL"
git config --global credential.helper 'cache --timeout 36000'

echo -e "\e[1;33m - Installation of Fun Tools \e[0m"
sudo apt-get install vlc -y
sudo apt-get install qbittorrent -y

echo -e "\e[1;33m - Full Clean of the system \e[0m"
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get full-upgrade -y
sudo apt-get autoremove -y
sudo apt-get autoclean -y

# todo (pilote carte graphique AMD® Renoir)
# https://www.amd.com/es/support/kb/release-notes/rn-amdgpu-unified-linux-20-20