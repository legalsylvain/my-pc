echo -e "\e[1;33m - Install Pywebdriver \e[0m"
cd /tmp/
wget https://raw.githubusercontent.com/akretion/pywebdriver/master/debian/install.sh
sudo chmod +x install.sh
sudo ./install.sh

echo -e "\e[1;33m - Create Dev repository \e[0m"
mkdir ~/grap_dev

echo -e "\e[1;33m - Get server-script Code \e[0m"
cd ~/grap_dev && git clone https://gitlab.com/grap-rhone-alpes/server-script
cd server-script
virtualenv env --python=python3
./env/bin/pip install -r requirements.txt
mkdir ~/grap_dev/backup/grp-production-odoo

echo -e "\e[1;33m - Get odoo-script Code \e[0m"
cd ~/grap_dev && git clone https://gitlab.com/grap-rhone-alpes/odoo-script

echo -e "\e[1;33m - Install Odoo 12.0 \e[0m"
cd ~/grap_dev/ && git clone https://gitlab.com/grap-rhone-alpes/grap-odoo-env -b 12.0
mv grap-odoo-env grap-odoo-env-12.0
cd ~/grap_dev/grap-odoo-env-12.0 && ./install.sh

echo -e "\e[1;33m - Install Odoo Eshop 12.0 \e[0m"
cd ~/grap_dev/ && git clone https://github.com/grap/odoo-eshop -b 12.0
mv odoo-eshop/ odoo-eshop-12.0
cd ~/grap_dev/odoo-eshop-12.0 && ./install.sh